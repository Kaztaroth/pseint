Proceso Reloj
	
	Definir Horas, Minutos, SegundosR como entero;
	Definir HorasC, MinutosC, SegundosC como caracter;
	Para Horas<-0 hasta 23 hacer
		Para Minutos <-0 hasta 59 hacer
			Para SegundosR <-0 hasta 59 hacer
				Borrar pantalla;
				Si Horas > 9 Entonces
					HorasC<-"";
				sino
					HorasC<-"0";
				FinSi
				Si Minutos > 9 Entonces
					MinutosC<-"";
				Sino
					MinutosC<-"0";
				FinSi
				Si SegundosR > 9 Entonces
					SegundosC <- "";
				Sino
					segundosC<-"0";
				FinSi
				Escribir HorasC, Horas,":",MinutosC, Minutos, ":",SegundosC, SegundosR;
				Esperar 15 Milisegundo;
			FinPara
		FinPara
	FinPara
FinProceso